library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.R_code_32_bit_pkg.ALL;

entity recovery is
    Port (proj : in natural;
          input_mrc : in set_mod_type_rec;
          output : out unsigned(num_bc-1 downto 0));
end recovery;

architecture Behavioral of recovery is
    signal mrc_temp : rec_type;
    
    Component add_tree_rec
        Generic (dinamic_count : natural);
        Port (input : in rec_type;
              output : out unsigned(num_bc-1 downto 0));
    end component;
    
begin
    mrc_temp(1) <= resize(input_mrc(1),mrc_temp(1)'length);
    mul : for i in 2 to count-2 generate
    begin
        mrc_temp(i) <= resize(input_mrc(i)*mod_mrc(proj)(i),mrc_temp(i)'length);
    end generate mul;
    
    num_last : add_tree_rec 
        Generic map (dinamic_count => count-2)
        Port map (input => mrc_temp,
                  output => output);

end Behavioral;
