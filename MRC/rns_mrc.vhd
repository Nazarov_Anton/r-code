library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.R_code_32_bit_pkg.ALL;

entity rns_mrc is
    Generic (constant proj : natural);
    Port (input_rns : in set_mod_type;
          output_mrc : out set_mod_type);
end rns_mrc;

architecture Behavioral of rns_mrc is
    signal b_loc : b_loc_type(1 to count*(count-1)/2);
    type mrc_tree is array (1 to count-1) of unsigned(2*mod_bc+count-2 downto 0);
    signal mrc_temp : mrc_tree;
    type carry_type is array (1 to count-2) of unsigned(2*mod_bc+count-2 downto 0);
    signal carry : carry_type;
    type mrc_temp_1_type is array (2 to count-1) of unsigned(2*mod_bc+count-2 downto 0);
    signal mrc_temp_1 : mrc_temp_1_type;
    
    Component add_tree 
        Generic (dinamic_count : natural);
        Port (input : in b_loc_type;
              output : out unsigned);
    end component;
    
    Component div_mod
        Generic (modul : unsigned(mod_bc-1 downto 0));
        Port (num : in unsigned(2*mod_bc+count-1 downto 0);
              quot : out unsigned(2*mod_bc+count-1 downto 0);
              remain : out unsigned(mod_bc-1 downto 0));
    end component;
    
    Component Mod_p
        Generic (p : unsigned(mod_bc-1 downto 0);
                 B : natural;
                 X_bc : natural);
        Port (X : in   unsigned(X_bc - 1 downto 0);
              X_mod_p : out  unsigned(p'length - 1 downto 0));
    end component;
       
begin

    b_mul_i : for j in 1 to count-1 generate
    begin
        b_mul_j : for k in j to count-1 generate
        begin
            b_loc(j+k*(k-1)/2) <= resize(b(proj)((count-1)*j-j*(j-1)/2-k)*input_rns(j), b_loc(j+k*(k-1)/2)'length);
        end generate b_mul_j;
    end generate b_mul_i;
    
    mrc_temp(1) <= resize(b_loc(1),mrc_temp(1)'length);
    sum_tree : for j in 2 to count-1 generate 
    begin
 --       b_loc_dinamic : for k in 1 to j generate 
 --       begin
 --           b_loc_tree(j)(k) <= b_loc(j*(j-1)/2+k); 
 --       end generate b_loc_dinamic;
        tree : add_tree 
            Generic map (dinamic_count => j)
            Port map (input => b_loc(j*(j+1)/2+1 to j*(j+1)/2+j),--b_loc_tree(j)
                      output => mrc_temp(j));     
    end generate sum_tree;
       
    mrc_last_1 : div_mod 
        Generic map (modul => m(proj)(1))
        Port map (num => mrc_temp(1),
                  quot => carry(1),
                  remain => output_mrc(1));
                  
        mrc_temp_1(2) <= mrc_temp(2) + carry(1);
       
    carry_out : for i in 2 to count-2 generate 
        begin
            mrc_last : div_mod 
                Generic map (modul => m(proj)(i))
                Port map (num => mrc_temp_1(i),
                          quot => carry(i),
                          remain => output_mrc(i));
                          
                mrc_temp_1(i+1) <= mrc_temp(i+1) + carry(i);
    end generate carry_out;
    
    mrc_last_count_1 : Mod_p 
        Generic map (p => m(proj)(count-1),
                     B => 4,
                     X_bc => 2*mod_bc+count-1)
        Port map (X => mrc_temp_1(count-1),
                  X_mod_p => output_mrc(count-1));  

end Behavioral;
