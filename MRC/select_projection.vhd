library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.R_code_32_bit_pkg.ALL;

entity select_projection is
    Port (proj : in unsigned(count-2 downto 0);
          output : out natural);
end select_projection;

architecture Behavioral of select_projection is

    constant zero : unsigned(count-2 downto 0) := (others => '0');
    constant one : unsigned(count-2 downto 0) := (others => '1');

	function getTopBit(x: unsigned) return natural is 
    begin
     for J in x'RANGE loop
        if x(J)='0' then
          return J;
        end if;
     end loop;
     return 0;
    end function getTopBit;
begin
    output <= 0 when (proj = zero or proj = one) else getTopBit(proj);
end Behavioral;
