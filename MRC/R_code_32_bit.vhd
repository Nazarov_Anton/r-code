library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.R_code_32_bit_pkg.ALL;

entity R_code_32_bit is
    Port (input : in mod_type;
          output : out unsigned(num_bc-1 downto 0));
end R_code_32_bit;

architecture Behavioral of R_code_32_bit is
    signal num_rns : array_set_mod_type;
    signal num_mrc : array_set_mod_type;
    signal projections : unsigned(count-2 downto 0) := (others => '0');
    signal projection : natural;
    signal num_mrc_rec : set_mod_type_rec;
    constant zero_mrc : unsigned(mod_bc-1 downto 0) := (others => '0');
    
    Component rns_mrc
        Generic (proj : natural);
        Port (input_rns : in set_mod_type;
              output_mrc : out set_mod_type);
    end component;
    
    Component recovery
        Port (proj : in natural;
              input_mrc : in set_mod_type_rec;
              output : out unsigned(num_bc-1 downto 0));
    end component;
    
    Component select_projection
        Port (proj : in unsigned(count-2 downto 0);
              output : out natural);
    end component;

begin

    detect_rns : for j in 1 to count-1 generate
    begin
        num_rns(0)(j) <= resize(input(j),num_rns(0)(j)'length);
    end generate detect_rns;
    
    localization_rns : for i in 1 to count-2 generate
    begin
        rns_i : for j in 1 to count-1 generate
        begin
            rns_j1 : if i>j generate begin num_rns(i)(j) <= resize(input(j),num_rns(i)(j)'length);
            end generate rns_j1;
            rns_jn : if (i<j or i=j) generate begin num_rns(i)(j) <= resize(input(j+1),num_rns(i)(j)'length);
            end generate rns_jn;
        end generate rns_i;
     end generate localization_rns;
     
     detect_localization : for i in 0 to count-2 generate
     begin
        mrc_projection : rns_mrc 
             Generic map (proj => i)
             Port map (input_rns => num_rns(i),
                       output_mrc => num_mrc(i));
     end generate detect_localization;

    process (num_mrc)
    variable pr : unsigned(count-2 downto 0);
    begin
        for i in 0 to count-2 loop
           if (num_mrc(i)(count-1) = zero_mrc) then 
               pr(i) := '1';
           else
               pr(i) := '0';
           end if;
        end loop;
    projections <= pr;
    end process;
    
   sel_proj : select_projection
            Port map (proj => projections,
                      output => projection);
                          
    mrc_rec : for i in 1 to count-2 generate
    begin
        num_mrc_rec(i) <= resize(num_mrc(projection)(i),num_mrc_rec(i)'length);
    end generate mrc_rec;
    
    rec : recovery
         Port map (proj => projection,
                   input_mrc => num_mrc_rec,
                   output => output);

end Behavioral;
