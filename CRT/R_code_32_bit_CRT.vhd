library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.R_code_32_bit_CRT_pkg.ALL;

entity R_code_32_bit_CRT is
    Port (input : in mod_type;
          output : out unsigned(num_bc-1 downto 0));
end R_code_32_bit_CRT;

architecture Behavioral of R_code_32_bit_CRT is
    signal num_rns : array_set_mod_type;
    signal num_crt : array_crt_proj;
    signal projections : unsigned(count-1 downto 0) := (others => '0');
    signal projection : natural;
    
    Component rns_crt
        Generic (proj : natural);
        Port (input_rns : in set_mod_type;
              output_crt : out unsigned((count-1)*mod_bc-1 downto 0));
    end component;
    
    Component select_projection
        Port (proj : in unsigned(count-1 downto 0);
              output : out natural);
    end component;
    
begin

    detect_proj : for j in 1 to count-1 generate
    begin
        num_rns(0)(j) <= resize(input(j),num_rns(0)(j)'length);
    end generate detect_proj;
    
    localization_proj : for i in 1 to count-1 generate
    begin
        rns_i : for j in 1 to count-1 generate
        begin
            rns_j1 : if i>j generate begin num_rns(i)(j) <= resize(input(j),num_rns(i)(j)'length);
            end generate rns_j1;
            rns_jn : if (i<j or i=j) generate begin num_rns(i)(j) <= resize(input(j+1),num_rns(i)(j)'length);
            end generate rns_jn;
        end generate rns_i;
    end generate localization_proj;
    
    calc_proj : for i in 0 to count-1 generate
    begin
        rnscrt : rns_crt
             Generic map (proj => i)
             Port map (input_rns => num_rns(i),
                       output_crt => num_crt(i));
    end generate calc_proj;

    process (num_crt)
    variable pr : unsigned(count-1 downto 0);
    begin
        for i in 0 to count-1 loop
           if (num_crt(i) < M_work) then 
               pr(i) := '1';
           else
               pr(i) := '0';
           end if;
        end loop;
    projections <= pr;
    end process;
    
   select_proj : select_projection
            Port map (proj => projections,
                      output => projection);
                      
    output <= resize(num_crt(projection),32);

end Behavioral;